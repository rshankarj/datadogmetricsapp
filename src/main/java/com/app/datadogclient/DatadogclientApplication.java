package com.app.datadogclient;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DatadogclientApplication {

    public static void main(String[] args) {
        SpringApplication.run(DatadogclientApplication.class, args);
    }

}
