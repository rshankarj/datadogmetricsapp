package com.app.datadogclient.helper;


import com.app.datadogclient.model.*;
import com.app.datadogclient.service.MetricsService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.net.URLCodec;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.text.SimpleDateFormat;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component
@Slf4j
public class DatadogApiClient {

    private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");

    private static final Logger logger = LoggerFactory.getLogger(MetricsService.class);
    private RestTemplateBuilder restTemplateBuilder;

    @Value("${datadog.uri}")
    private String datadogBaseUrl;

    @Value("${datadog.host.uri}")
    private String dataDogHostUrl;

    @Value("${datadog.api_key}")
    private String api_key;

    @Value("${datadog.application_key}")
    private String application_key;

    @Autowired
    RestTemplate restTemplate = new RestTemplate();
    String queryForCpuUtilization = "avg:system.cpu.user{*}";

    String datadogUrl;

    @Autowired
    public DatadogApiClient(RestTemplateBuilder restTemplateBuilder){
        // @Value("${fakestore.api.url}") String fakestoreurl) {
        this.restTemplateBuilder = restTemplateBuilder;
        //this.specificProductUrl = fakestoreurl;
    }

    public MetricsData getMetricsByHostName(String startDate ,String endDate,String hostName){
        MetricsData metricsData= new MetricsData();
        DataDogResponse dataDogResponse= new DataDogResponse();
        try {
            dataDogResponse = getMetricsByHost(startDate, endDate, hostName);
            metricsData=setCpuUtilizationDataDogMetricsDto(dataDogResponse);
        }
        catch (Exception exp){

        }
        return  metricsData;
    }

    public DataDogResponse getSystemCPUMetrics(String from , String to, String queryString) throws UnsupportedEncodingException {
        DataDogResponse dataDogResponse = new DataDogResponse();
        URLCodec codec,urlCodec;
        try {
            String cpuUtilizationUrlWithQuery = uriHttpBuilder(datadogBaseUrl,queryString, formatDate(from), formatDate(to));
            String tag="{*}";
            logger.info(""+cpuUtilizationUrlWithQuery);
            datadogUrl = URLDecoder.decode(cpuUtilizationUrlWithQuery+tag);
            logger.info("final URL =============>>>> "+datadogUrl);
            RestTemplate restTemplate = restTemplateBuilder.build();
            ResponseEntity<DataDogResponse>
            responseEntity=restTemplate.getForEntity(datadogUrl,DataDogResponse.class, tag);
            if (responseEntity.getStatusCode().is2xxSuccessful()) {
                dataDogResponse = responseEntity.getBody();
                List<SeriesData> seriesData = dataDogResponse != null ? dataDogResponse.getSeries() : null;
                if (CollectionUtils.isEmpty(seriesData)) {
                    logger.warn("No data found where requested for start {} and end {} date.", from, to);
                }
                logger.info("Successfully fetched cpu utilization from dataDog.");
            } else {
                logger.warn("Error occurred while retrieving metrics from Datadog with this status code: {}", responseEntity.getStatusCode());
            }


        } catch (RestClientException e) {
            throw new RuntimeException(e);
        }
        return dataDogResponse;
    }

    // Metrics By Host Name :



    private DataDogResponse getMetricsByHost(String from , String to, String hostname) throws IOException {
        DataDogResponse hostMetricsResponse;
        String hostQuery = String.format("avg:system.cpu.user{host:%s}",hostname);
        String hostCpuUtilizationUrlWithQuery = uriHttpBuilder(datadogBaseUrl,hostQuery,formatDate(from), formatDate(to));
        logger.info(""+hostCpuUtilizationUrlWithQuery);
        String hostMetricsUrl = URLDecoder.decode(hostCpuUtilizationUrlWithQuery);
        logger.info("final URL =============>>>> "+hostMetricsUrl);
        hostMetricsResponse = new DataDogResponse();
        ResponseEntity<DataDogResponse>
                responseEntity=restTemplate.getForEntity(hostMetricsUrl,DataDogResponse.class,String.format("{host:%s}",hostname));
        hostMetricsResponse = responseEntity.getBody();
/*        List<SeriesData> seriesData =
                hostMetricsResponse !=null ? hostMetricsResponse.getSeries() : null;
        if (CollectionUtils.isEmpty(seriesData)) {
            logger.warn("No data found where requested for start {} and end {} date.",from, to);
        }
        ObjectMapper objectMapper = new ObjectMapper();
        String jsonResponse = objectMapper.writeValueAsString(responseEntity);*/
        logger.info("",responseEntity.getBody());

        // Parse JSON response
        //return parseCpuUtilization(jsonResponse);
        return hostMetricsResponse;
    }

    private String uriHttpBuilder(String ddUrl,String query, Long from, Long to) {
        return UriComponentsBuilder.fromHttpUrl(ddUrl)
                .queryParam("api_key", api_key)
                .queryParam("application_key", application_key)
                .queryParam("from", from)
                .queryParam("to", to)
                .queryParam("query", query)
                .toUriString();
    }

    //Date Formatter
    private long formatDate(String startDate) {
        LocalDate localDate = LocalDate.
                parse
                        (startDate);
        ZonedDateTime zonedDateTime = localDate.atStartOfDay(ZoneId.
                systemDefault
                        ());
        Instant instant = zonedDateTime.toInstant();
        return
                instant.getEpochSecond();
    }

    private MetricsData setCpuUtilizationDataDogMetricsDto(DataDogResponse metricsResponse) {
        MetricsData ddMetricsDto = new MetricsData();
        ddMetricsDto.setMetricsType(metricsResponse.getResourceType());
        ddMetricsDto.setStartDate(formDateToString(metricsResponse.getStartDate()));
        ddMetricsDto.setEndDate(formDateToString(metricsResponse.getEndDate()));
        ddMetricsDto.setCpuUtilization(transformMetricsData(metricsResponse));
        return ddMetricsDto;
    }

    private List<ValueAndDate> transformMetricsData(DataDogResponse response) {
        List<ValueAndDate> valueAndDateList = new ArrayList<>();
        List<SeriesData> seriesDataList = response.getSeries();

        for (SeriesData data : seriesDataList) {
            for (List<Object> valueList : data.getPointlist()) {
                ValueAndDate valueAndDate = new ValueAndDate();
                double unixSeconds = (double) valueList.get(0);
                Date date = new Date((long) (unixSeconds));
                String timestamp = sdf.format(date);

                valueAndDate.setTimestamp(timestamp);
                valueAndDate.setValue(Double.valueOf(valueList.get(1).toString()));
                valueAndDateList.add(valueAndDate);
            }
        }
        return valueAndDateList;
    }

    private String formDateToString(long fromDate){
        Instant instant = Instant.ofEpochMilli(fromDate);

        // Convert Instant to LocalDateTime
        LocalDateTime localDateTime = LocalDateTime.ofInstant(instant, ZoneId.systemDefault());

        // Define the desired date format
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/yyyy HH:mm:ss");

        // Format the LocalDateTime object to a string
        return localDateTime.format(formatter);

    }

}
