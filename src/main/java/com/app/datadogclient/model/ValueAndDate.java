package com.app.datadogclient.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

@ToString
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ValueAndDate {
    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    private Double value;

    @Override
    public String toString() {
        return "ValueAndDate{" +
                "value=" + value +
                ", timestamp='" + timestamp + '\'' +
                '}';
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    private String timestamp;

}

