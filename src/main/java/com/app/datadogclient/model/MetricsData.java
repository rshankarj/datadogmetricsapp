package com.app.datadogclient.model;

import java.util.List;

public class MetricsData {
    String metricsType;

    String startDate;
    String endDate;
    List<ValueAndDate> cpuUtilization;

    public String getMetricsType() {
        return metricsType;
    }

    public void setMetricsType(String metricsType) {
        this.metricsType = metricsType;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public List<ValueAndDate> getCpuUtilization() {
        return cpuUtilization;
    }

    public void setCpuUtilization(List<ValueAndDate> cpuUtilization) {
        this.cpuUtilization = cpuUtilization;
    }
}
