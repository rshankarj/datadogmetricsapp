package com.app.datadogclient.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.*;

import java.util.ArrayList;
import java.util.List;


public class SeriesData {

    private ArrayList<UnitData> unit;
    private int query_index;
    private String aggr;
    private String metric;
    private ArrayList<Object> tag_set;
    private String expression ;
    private String scope;
    private int interval;
    private int length;
    private long start;
    private long end;

    private List<List<Object>> pointlist;
    private String display_name;


    public ArrayList<UnitData> getUnit() {
        return unit;
    }

    public void setUnit(ArrayList<UnitData> unit) {
        this.unit = unit;
    }

    public int getQuery_index() {
        return query_index;
    }

    public void setQuery_index(int query_index) {
        this.query_index = query_index;
    }

    public String getAggr() {
        return aggr;
    }

    public void setAggr(String aggr) {
        this.aggr = aggr;
    }

    public String getMetric() {
        return metric;
    }

    public void setMetric(String metric) {
        this.metric = metric;
    }

    public ArrayList<Object> getTag_set() {
        return tag_set;
    }

    public void setTag_set(ArrayList<Object> tag_set) {
        this.tag_set = tag_set;
    }

    public String getExpression() {
        return expression;
    }

    public void setExpression(String expression) {
        this.expression = expression;
    }

    public String getScope() {
        return scope;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }

    public int getInterval() {
        return interval;
    }

    public void setInterval(int interval) {
        this.interval = interval;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public long getStart() {
        return start;
    }

    public void setStart(long start) {
        this.start = start;
    }

    public long getEnd() {
        return end;
    }

    public void setEnd(long end) {
        this.end = end;
    }

    public List<List<Object>> getPointlist() {
        return pointlist;
    }

    public void setPointlist(List<List<Object>> pointlist) {
        this.pointlist = pointlist;
    }

    public String getDisplay_name() {
        return display_name;
    }

    public void setDisplay_name(String display_name) {
        this.display_name = display_name;
    }
}
