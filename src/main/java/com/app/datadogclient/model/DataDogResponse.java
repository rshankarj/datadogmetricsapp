package com.app.datadogclient.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;
import java.util.ArrayList;

public class DataDogResponse {

    private String status;
    private String resourceType;
    private float responseVersion;
    private String query;
    private long startDate;
    private long endDate;
    private ArrayList<SeriesData> series ;
    private ArrayList <Object> values ;
    private ArrayList <Object> times ;
    private String message;
    private ArrayList<Object> group_by;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getResourceType() {
        return resourceType;
    }

    public void setResourceType(String resourceType) {
        this.resourceType = resourceType;
    }

    public float getResponseVersion() {
        return responseVersion;
    }

    public void setResponseVersion(float responseVersion) {
        this.responseVersion = responseVersion;
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public long getStartDate() {
        return startDate;
    }

    public void setStartDate(long startDate) {
        this.startDate = startDate;
    }

    public long getEndDate() {
        return endDate;
    }

    public void setEndDate(long endDate) {
        this.endDate = endDate;
    }

    public ArrayList<SeriesData> getSeries() {
        return series;
    }

    public void setSeries(ArrayList<SeriesData> series) {
        this.series = series;
    }

    public ArrayList<Object> getValues() {
        return values;
    }

    public void setValues(ArrayList<Object> values) {
        this.values = values;
    }

    public ArrayList<Object> getTimes() {
        return times;
    }

    public void setTimes(ArrayList<Object> times) {
        this.times = times;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<Object> getGroup_by() {
        return group_by;
    }

    public void setGroup_by(ArrayList<Object> group_by) {
        this.group_by = group_by;
    }
}
