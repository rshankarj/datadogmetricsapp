package com.app.datadogclient.controller;



import com.app.datadogclient.model.MetricsData;
import com.app.datadogclient.service.MetricsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

@RestController

public class DatadogController {

    @Autowired
   private MetricsService metricsService;

    @GetMapping("/metricsByHost")
    public MetricsData getMetricsByHost(@RequestParam String  from, @RequestParam String  to, @RequestParam String hostName) throws IOException {
        return metricsService.getMetricsByHost(from,to,hostName);
   }

}
