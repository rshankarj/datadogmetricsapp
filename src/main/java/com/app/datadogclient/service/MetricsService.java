package com.app.datadogclient.service;


import com.app.datadogclient.helper.DatadogApiClient;
import com.app.datadogclient.model.*;

import lombok.extern.slf4j.Slf4j;


import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
@Slf4j
public class MetricsService {

    @Autowired
    DatadogApiClient datadogApiClient;

    public MetricsData getMetricsByHost(String from , String to, String hostname) throws IOException {
       return  datadogApiClient.getMetricsByHostName(from, to , hostname);
    }




}



